﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using System.IO;
using SQLite;
using AppLoginSqlLite.Resources.Model;
using Android.Content;

namespace AppLoginSqlLite
{
    [Activity(Label = "AppLoginSqlLite", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        EditText txtusuario;
        EditText txtSenha;
        Button btnCriar;
        Button btnLogin;
        string DbPath { get { return Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Usuario.db3"); } }


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            btnLogin = FindViewById<Button>(Resource.Id.btnLogin);
            btnCriar = FindViewById<Button>(Resource.Id.btnRegistrar);
            txtusuario = FindViewById<EditText>(Resource.Id.editTextLogin);
            txtSenha = FindViewById<EditText>(Resource.Id.editTextSenha);

            CriarBancoDeDados();
            btnLogin.Click += BtnLogin_Click;
            btnCriar.Click += BtnCriar_Click;
            
        }

        private void CriarBancoDeDados()
        {
            try
            {
                var db = new SQLiteConnection(DbPath);               
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.ToString(), ToastLength.Short).Show();
            }
        }

        private void BtnCriar_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(NovoUsuarioActivity));
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                var db = new SQLiteConnection(DbPath);
                var dados = db.Table<Login>();

                var login = dados.Where(w => w.usuario.Equals(txtusuario.Text) && w.senha.Equals(txtSenha.Text)).FirstOrDefault();

                if (login != null)
                {
                    Toast.MakeText(this, "Login realizado com sucesso!", ToastLength.Short).Show();

                    var loginActivity = new Intent(this, typeof(LoginActivity));
                    loginActivity.PutExtra("nome", FindViewById<EditText>(Resource.Id.editTextLogin).Text);
                    StartActivity(loginActivity);
                }
                else
                {
                    Toast.MakeText(this, "Usuário e/ou senha inválido(s)!", ToastLength.Short).Show();
                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
            }
        }
    }
}

