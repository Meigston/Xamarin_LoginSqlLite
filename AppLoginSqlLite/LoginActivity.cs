using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppLoginSqlLite
{
    [Activity(Label = "LoginActivity")]
    public class LoginActivity : Activity
    {
        TextView txtNome;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Login);
            try
            {
                txtNome = FindViewById<TextView>(Resource.Id.txtNome);
                txtNome.Text = string.Format("{0} - {1:dd/MM/yyyy HH:mm:ss}", Intent.GetStringExtra("nome") ?? "Visitante", DateTime.Now);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
            }            
        }
    }
}