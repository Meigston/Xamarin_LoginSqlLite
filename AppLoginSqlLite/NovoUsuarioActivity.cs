using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using SQLite;
using AppLoginSqlLite.Resources.Model;

namespace AppLoginSqlLite
{
    [Activity(Label = "NovoUsuarioActivity")]
    public class NovoUsuarioActivity : Activity
    {
        EditText txtNovoUsuario;
        EditText txtSenhaNovoUsuario;
        Button btnCriarNovoUsuario;
        string DbPath { get { return Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Usuario.db3"); } }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.NovoUsuario);

            btnCriarNovoUsuario = FindViewById<Button>(Resource.Id.btnCriarUsuario);
            txtNovoUsuario = FindViewById<EditText>(Resource.Id.editTextUsuario);
            txtSenhaNovoUsuario = FindViewById<EditText>(Resource.Id.editTextSenha);

            btnCriarNovoUsuario.Click += BtnCriarNovoUsuario_Click;
        }

        private void BtnCriarNovoUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                var db = new SQLiteConnection(DbPath);

                if (db.Table<Login>().Table == null)
                {
                    db.CreateTable<Login>();
                }
                
                Login tbLogin = new Login();

                tbLogin.usuario = txtNovoUsuario.Text;
                tbLogin.senha = txtSenhaNovoUsuario.Text;

                db.Insert(tbLogin);

                Toast.MakeText(this, "Usu�rio cadastrado com sucesso!", ToastLength.Short).Show() ;
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
            }
        }
    }
}